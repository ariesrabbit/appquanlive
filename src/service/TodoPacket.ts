import { collection, getDoc, getDocs } from "firebase/firestore";
import {db} from "../firebase/database";


export type TodoPacket = {
    id?: string;
    BookingCode:string,
    GiaVe: number;
    GiaCombo: number;
    NgayAD: any;
    NgayHH:any;
    TenGoiVe:string;
    SoLuong:number;
    TinhTrangGoi:string;
};

export const all = () => async () => {
    // const snapshot = await db.collection("BookingPacket").get();
    const bookRef = collection(db,"BookingPacket");
    // const data: Array<any> = [];
    const result = await getDocs(bookRef);

};


