import React, { useEffect, useState } from "react";
import "./index.css";

import { SearchOutlined, FilterOutlined } from "@ant-design/icons";

import * as todo from "../../service/todo";
import { all } from "../../service/todo";
import { useDispatch, useSelector } from "react-redux";
import { bindActionCreators } from "redux";
import { bookingCreator } from "../../redux";
import { State } from "../../redux/reducer";


export const DanhSachVe = () => {
    const dispatch = useDispatch();

    const { LoadDuLieu } = bindActionCreators(bookingCreator, dispatch);

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
        // fetchTodos();
        LoadDuLieu();
    }, []);

    const { listBooking } = useSelector((state: State) => state.booking);

    const [todos, setTodos] = useState([{
        BookingCode: '',
        NgaySuDung: '',
        NgayXuatVe: '',
        TenSuKien: '',
        SoVe: '',
        CongCheckIn: '',
        TinhTrang: '',
    }]);

    useEffect(() => {
        setTodos(listBooking);
    }, [listBooking])


    return (
        
        <div>
           
            <div className="DanhSach">
                <div className="DanhSach-tk">
                    <h2>Danh Sách Vé</h2>
                    <div className="DanhSach-Ve">
                        <div className="search">
                            <input type="text" placeholder='Search' />
                            <a href="/"><SearchOutlined /></a>
                        </div>
                        <div className="DanhSach-locVe">
                            <div className="DanhSach-locVe1"><button className="LocVe" data-toggle='modal' data-target="#themGoiVe" ><FilterOutlined /> Lọc Vé</button></div>
                            <div className="DanhSach-locVe1"><button className="XuatFile" > Xuất file(.csv)</button></div>
                        </div>
                    </div>

                    <table className="table-danhsachve">
                        <thead>
                            <tr>
                                <th>STT</th>
                                <th>Booking code</th>
                                <th>Số vé</th>
                                <th>Tên sự kiện</th>
                                <th>Tình trạng</th>
                                <th>Ngày sử dụng</th>
                                <th>Ngày xuất vé</th>
                                <th>Cổng check-in</th>
                            </tr>
                        </thead>
                        <tbody>
                            {todos.map((todos, index) => (
                                <tr>
                                    <td className="cangiua">{index + 1}</td>
                                    <td className="cantrai">{todos.BookingCode}</td>
                                    <td className="canphai">{todos.SoVe}</td>
                                    <td className="canphai">{todos.TenSuKien}</td>
                                    <td className="cantrai">
                                        {(() => {

                                            if (todos.TinhTrang === 'Hết hạn') {

                                                return (

                                                    <div style={{ border: '0.5px solid #FD5959', backgroundColor: '#FFC0CB', borderRadius: 5, color: 'red', fontSize: '0.875em', height: 30, padding: 3 }}>
                                                        Hết hạn
                                                    </div>

                                                )

                                            } else if (todos.TinhTrang === 'Chưa sử dụng') {

                                                return (

                                                    <div style={{ border: '0.5px solid #03AC00', backgroundColor: '#98FB98', borderRadius: 5, color: '#03AC00', fontSize: '0.875em', height: 30, padding: 3 }}>
                                                        Chưa sử dụng
                                                    </div>

                                                )

                                            } else if (todos.TinhTrang === 'Đã sử dụng') {

                                                return (

                                                    <div style={{ border: '0.5px solid #919DBA', backgroundColor: '#EAF1F8', borderRadius: 5, color: '#919DBA', fontSize: '0.875em', height: 30, padding: 3 }}>
                                                        Đã sử dụng
                                                    </div>

                                                )

                                            }

                                        })()}


                                    </td>
                                    <td className="cantrai">{todos.NgaySuDung}</td>
                                    <td className="cantrai">{todos.NgayXuatVe}</td>
                                    <td className="cantrai">{todos.CongCheckIn}</td>
                                </tr>
                            )
                            )
                            }
                        </tbody>
                    </table>

                </div>
            </div>
            <div className="modal" id="themGoiVe" role="modal" style={{ marginLeft: -170}}>
                <div className="modal-dialog">
                    <div className="modal-content">

                        <div className="BangThemVe" style={{height:500}}>
                            <h2>Lọc vé</h2>
                            <div className="ThemVe-Date">
                                <div style={{marginRight:150}}  className="date1">
                                    <h4>Từ ngày</h4>
                                    <input style={{ borderRadius: 10, border: '1px solid #A5A8B1' }} className="Date-Them" type="date" />
                                </div>
                                <div  className="date2">
                                    <h4>Đến ngày</h4>
                                    <input style={{ borderRadius: 10, border: '1px solid #A5A8B1' }} className="Date-Them" type="date" />
                                </div>
                            </div>
                            <div className="ThemVe-GiaVe">
                                <h4><b>Tình trạng sử dụng</b></h4>
                                <div style={{marginLeft:-32,display:'flex'}} className="TinhTrangDoiSoat-item">
                                    <div className="item-input"><input  type="radio" name="option" /> Tất Cả</div>
                                    <div className="item-input"><input type="radio" name="option" /> Đã sử dụng</div>
                                    <div className="item-input"><input type="radio" name="option" /> Chưa sử dụng</div>
                                    <div className="item-input"><input type="radio" name="option" /> Hết hạn</div>
                                </div>
                            </div>
                            <div className="ThemVe-TinhTrang">
                                <h4><b>Cổng checkin</b> </h4>
                                <div style={{display:'flex', fontSize:20 }}>
                                    <div>
                                        <div><input type="checkbox" /> Tất cả</div>
                                        <div><input type="checkbox" /> Cổng 3</div>
                                    </div >
                                    <div style={{marginLeft:100}}>
                                        <div><input type="checkbox" /> Cổng 1</div>
                                        <div><input type="checkbox" /> Cổng 4</div>
                                    </div>
                                    <div style={{marginLeft:100}}>
                                        <div><input type="checkbox" /> Cổng 2</div>
                                        <div><input type="checkbox" /> Cổng 5</div>
                                    </div>
                                    
                                   
                                </div>
                                
                            </div>
                            <p style={{ color: "#A5A8B1", width: 300 }} className="Batbuoc"><span>*</span>là thông tin bắt buộc</p>
                            <div className="btn-ThemVe">
                                <button className="btn-huy"><b>Lọc</b></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}