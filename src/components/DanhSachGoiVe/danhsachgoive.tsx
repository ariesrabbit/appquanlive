import React, { useEffect, useState } from "react";
import "./index.css";

import { EditOutlined, SearchOutlined } from "@ant-design/icons";
// import * as todoPackets  from "../../service/TodoPacket";
import { CSVLink } from "react-csv";
import { useDispatch, useSelector } from "react-redux";
import { LoadDuLieu } from "../../redux/actionCreator/todoPacketCreator";
import { bindActionCreators } from "redux";
import { todoPacketCreator } from "../../redux";
import { toDoPacket } from "../../service";
import { State } from "../../redux/reducer";

function GoiDichVu(){

    const dispatch = useDispatch();

    const {LoadDuLieu} = bindActionCreators(todoPacketCreator, dispatch);

    useEffect(() => {
        window.scrollTo({ top: 0, behavior: "smooth" });
        // fetchTodoPacket();
        LoadDuLieu();
      }, []);

      const {listToDoPacket} = useSelector((state: State) => state.todoPacKet);

    //   const ShowThemVe = () =>{
    //     const element: HTMLElement = document.getElementById('Background-black1') as HTMLElement
    //     element.style.display = 'block'
    //  }
     const HuyThemVe = () =>{
        const element: HTMLElement = document.getElementById('Background-black1') as HTMLElement
        element.style.display = 'none'
     }
     const [todoPacket, setTodoPacket] = useState<Array<toDoPacket.TodoPacket>>([]);


     useEffect(()=> {
        console.log('list', listToDoPacket);
        setTodoPacket(listToDoPacket);
     }, [listToDoPacket])

    
    const headers =[
        { label: "id", key: "id" },
        { label: "TenGoiVe", key: "TenGoiVe" },
        { label: "NgayApDung", key: "NgayAD" },
        { label: "NgayHetHan", key: "NgayHH" },
        { label: "GiaVe", key: "GiaVe" },
        { label: "GiaCombo", key: "GiaVe" },
        { label: "GiaCombo", key: "GiaVe" },
        { label: "TinhTrangGoi", key: "TinhTrangGoi" },
        { label: "SoLuong", key: "SoLuong" },
        
     ]

     const cvreport = {
        data: todoPacket,
        headers: headers,
        filename: 'XuatGoiVe.csv'
     }
   return(
     <div>
            <div className="DanhSach">
            <div className="DanhSach-tk">
                <h2>Danh Sách Vé</h2>
                <div className="DanhSach-Ve">
                <div className="search">
                    <input  type="text" placeholder='Search'  />
                    <a href="/"><SearchOutlined /></a>
                 </div> 
                    <div className="DanhSach-locVe">
                    <div className="DanhSach-locVe1"><button className="XuatFile" > Xuất file(.csv)</button></div>
                        <div className="DanhSach-locVe1">
                            <button className="ThemVe" data-toggle='modal' data-target="#themGoiVe">Thêm gói vé</button>
                        </div>       
                    </div>
                </div>
                <table className="table-danhsachgoive">
                <thead>
                    <tr>
                        <th>STT</th>
                        <th className="cantrai">Mã gói</th>             
                        <th className="cantrai">Tên gói vé</th>
                        <th className="canphai">Ngày áp dụng</th>
                        <th className="canphai">Ngày hết hạn</th>
                        <th className="canphai">Giá vé  (VNĐ/Vé)</th>
                        <th className="cantrai">Giá Combo <br />(VNĐ/Combo)</th>
                        <th className="cantrai">Tình Trạng</th>
                        <th> </th>
                    </tr>
                </thead>    
                <tbody>
                {todoPacket.map((todopacket, index) => (
                        <tr key={index}>
                        <td>{index + 1}</td>
                        <td className="cantrai">{todopacket.BookingCode}</td>
                        <td className="cantrai">{todopacket.TenGoiVe}</td>
                        <td className="canphai">{todopacket.NgayAD}</ td>
                        <td style={{width:70}} className="canphai">{todopacket.NgayHH}</td>
                        <td style={{width:70}} className="canphai">{todopacket.GiaVe}</td>
                        <td  className="cantrai">{todopacket.GiaCombo}</td>
                        <td style={{width:150}} className="cantrai"><div className="CheckTinhTrang">{(() => {

                            if (todopacket.TinhTrangGoi === 'Tắt') {

                                return (

                                    <div style={{ border: '0.5px solid #FD5959', backgroundColor: '#FFC0CB', borderRadius: 5, color: 'red', fontSize: '0.875em', height: 30, padding: 3 }}>
                                        Tắt
                                    </div>

                                )

                            } else if (todopacket.TinhTrangGoi === 'Đang áp dụng') {

                                return (

                                    <div style={{ border: '0.5px solid #03AC00', backgroundColor: '#98FB98', borderRadius: 5, color: '#03AC00', fontSize: '0.875em', height: 30, padding: 3 }}>
                                        Đang áp dụng
                                    </div>

                                )

                            } 
                            

                            })()}
                            </div></td>
                        <td><button style={{width:150}} className="btn-capnhap"><EditOutlined className="EditOutlined" /> Cập nhập</button></td>
                    </tr>
                 ))}
                   
                    
                </tbody>
                </table>

            </div>
            <div id="Background-black1">
                <div className="BangThemVe">
                    <h2>Thêm gói vé</h2>
                    <div className="ThemVe-Ten">
                        <h4>Tên gói vé <span>*</span></h4>
                        <input type="text" placeholder="Nhập tên gói vé"/>
                    </div>
                    <div className="ThemVe-Date">
                        <div className="date1">
                            <h4>Ngày áp dụng</h4>
                            <input className="Date-Them" type="date" />
                            <input type="time" />
                        </div>
                        <div className="date2">
                            <h4>Ngày hết hạn</h4>
                            <input className="Date-Them" type="date" />
                            <input type="time" />
                        </div>
                    </div>
                   
                    <div className="ThemVe-GiaVe">
                        <h4>Giá vé áp dụng</h4>
                        <div className="Ve-Le">
                            <input type="checkbox" /> Vé lẻ (vnđ/vé) với giá <input className="text-ve" placeholder="Giá vé" type="text" /> / vé
                        </div>
                        <div className="Ve-Combo">
                            <input type="checkbox" /> Combo vé với giá <input className="text-ve" placeholder="Giá vé" type="text" /> / <input className="text-ve1" placeholder="Giá vé" type="text" /> vé
                        </div>
                    </div>
                    <div className="ThemVe-TinhTrang">
                        <h4>Tình Trạng</h4>
                        <select className="Cbm">
                            <option value="Đang áp dụng">Đang áp dụng</option>
                            <option value="Chưa áp dụng">Chưa áp dụng</option>
                        </select>
                    </div>
                    <p className="Batbuoc"><span>*</span>là thông tin bắt buộc</p>
                    <div className="btn-ThemVe">
                        <button className="btn-huy" onClick={HuyThemVe}>Hủy</button>
                        <button className="btn-them">Lưu</button>
                    </div>
                </div>
            </div>
        </div>
        <div className="modal" id="themGoiVe" role="modal" style={{marginLeft:-170}}>
            <div className="modal-dialog">
                <div className="modal-content">
                    
                        <div className="BangThemVe">
                    <h2>Thêm gói vé</h2>
                    <div className="ThemVe-Ten">
                        <h4>Tên gói vé <span>*</span></h4>
                        <input type="text" placeholder="Nhập tên gói vé"/>
                    </div>
                    <div className="ThemVe-Date">
                        <div className="date1">
                            <h4>Ngày áp dụng</h4>
                            <input style={{borderRadius:10,  border: '1px solid #A5A8B1'}} className="Date-Them" type="date" />
                            <input style={{borderRadius:10, marginRight:40,border: '1px solid #A5A8B1'}} type="time" />
                        </div>
                        <div className="date2">
                            <h4>Ngày hết hạn</h4>
                            <input style={{borderRadius:10, border: '1px solid #A5A8B1'}} className="Date-Them" type="date" />
                            <input style={{borderRadius:10, border: '1px solid #A5A8B1'}} type="time" />
                        </div>
                    </div>
                    <div className="ThemVe-GiaVe">
                        <h4>Giá vé áp dụng</h4>
                        <div className="Ve-Le">
                            <input type="checkbox" /> Vé lẻ (vnđ/vé) với giá <input className="text-ve" placeholder="Giá vé" type="text" /> / vé
                        </div>
                        <div className="Ve-Combo">
                            <input type="checkbox" /> Combo vé với giá <input className="text-ve" placeholder="Giá vé" type="text" /> / <input style={{width:90}} className="text-ve1" placeholder="Giá vé" type="text" /> vé
                        </div>
                    </div>
                    <div className="ThemVe-TinhTrang">
                        <h4>Tình Trạng</h4>
                        <select className="Cbm">
                            <option value="Đang áp dụng">Đang áp dụng</option>
                            <option value="Chưa áp dụng">Chưa áp dụng</option>
                        </select>
                    </div>
                    <p style={{color:"#A5A8B1", width:300}} className="Batbuoc"><span>*</span>là thông tin bắt buộc</p>
                    <div className="btn-ThemVe">
                        <button className="btn-huy" onClick={HuyThemVe}>Hủy</button>
                        <button className="btn-them">Lưu</button>
                    </div>
                </div>
                    </div>
                </div>
        </div>
        </div>
       
   )
}

export default GoiDichVu;