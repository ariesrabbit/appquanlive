import Item from "antd/lib/list/Item";
import { ActionProps } from "../action/todoPacketAction"

const initialState = {
    listToDoPacket: []
}

export const todoPacketReducer = (state:any = initialState, action: ActionProps) => {
    switch(action.type) {
        case 'LOAD_DU_LIEU': {
            console.log('Action.payload', action.payload);
            const result = action.payload.docs.map((item:any) => ({...item.data(), id: item.id}));
            state.listToDoPacket = result;
            return {...state};
            break;
        }
        default: return state;
    }
} 