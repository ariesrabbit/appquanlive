import { combineReducers, createStore } from "redux";
import { BookingReducer } from "./BookingReducer";
import { todoPacketReducer } from "./todoPacketReducer";

const reducers = combineReducers({
    booking: BookingReducer,
    todoPacKet: todoPacketReducer
})

export default reducers;

export type State = ReturnType<typeof reducers>;