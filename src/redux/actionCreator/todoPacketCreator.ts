import { collection, getDocs } from "firebase/firestore";
import { Dispatch } from "react";
import { db } from "../../firebase/database";
import { ActionProps } from "../action/todoPacketAction";

export const LoadDuLieu = () => async (dispatch: Dispatch<ActionProps>) => {
    // const snapshot = await db.collection("BookingPacket").get();
    try {
        const bookRef = collection(db,"booking");
        // const data: Array<any> = [];
        const result = await getDocs(bookRef);
        dispatch({
            type: 'LOAD_DU_LIEU',
            payload: result
        })
    }
    catch(error) {
        console.log('Loi roi !');
    }

    // eslint-disable-next-line array-callback-return
    // snapshot.docs.map((_data) => {
    //     data.push({
    //         id: _data.id, 
    //         ..._data.data(), 
    //     });
    // });

    // return data as Array<TodoPacket>;
};